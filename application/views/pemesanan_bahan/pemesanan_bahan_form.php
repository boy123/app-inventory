
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Bahan Baku <?php echo form_error('id_bahan_baku') ?></label>
            <!-- <input type="text" class="form-control" name="id_bahan_baku" id="id_bahan_baku" placeholder="Id Bahan Baku" value="<?php echo $id_bahan_baku; ?>" /> -->
            <select name="id_bahan_baku" class="form-control select2">
                <option value="<?php echo $id_bahan_baku ?>"><?php echo $id_bahan_baku ?></option>
                <?php foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_bahan_baku ?>"><?php echo $rw->bahan_baku ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Frekuensi <?php echo form_error('frekuensi') ?></label>
            <input type="text" class="form-control" name="frekuensi" id="frekuensi" placeholder="Frekuensi" value="<?php echo $frekuensi; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Biaya Transportasi <?php echo form_error('biaya_transportasi') ?></label>
            <input type="text" class="form-control" name="biaya_transportasi" id="biaya_transportasi" placeholder="Biaya Transportasi" value="<?php echo $biaya_transportasi; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Total Biaya <?php echo form_error('total_biaya') ?></label>
            <input type="text" class="form-control" name="total_biaya" id="total_biaya" placeholder="Total Biaya" value="<?php echo $total_biaya; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Lead Time <?php echo form_error('lead_time') ?></label>
            <input type="text" class="form-control" name="lead_time" id="lead_time" placeholder="Lead Time" value="<?php echo $lead_time; ?>" />
        </div>
	    <input type="hidden" name="id_pemesanan" value="<?php echo $id_pemesanan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('pemesanan_bahan') ?>" class="btn btn-default">Cancel</a>
	</form>
   