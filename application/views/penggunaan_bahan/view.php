<div class="row">
	<div class="col-md-12">
		<p>
			<a href="penggunaan_bahan" class="btn btn-primary">Tambah Data</a>
		</p>
		<div class="table-responsive">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th rowspan="3" width="50">No.</th>
					<th rowspan="3">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td colspan="2"><?php echo $rw->bahan_baku ?></td>
						<?php endforeach ?>
					</tr>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td>Penggunan Maksimal</td>
							<td>Penggunan Rata-Rata</td>
						<?php endforeach ?>
					</tr>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>
					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('penggunaan_bahan',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php echo $value->maksimal ?></td>
						<td><?php echo $value->rata_rata ?></td>
					<?php endforeach ?>
				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>
	</div>
</div>