
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Bulan <?php echo form_error('id_bulan') ?></label>
            <!-- <input type="text" class="form-control" name="id_bulan" id="id_bulan" placeholder="Id Bulan" value="<?php echo $id_bulan; ?>" /> -->
            <select name="id_bulan" class="form-control">
                <option value="<?php echo $id_bulan ?>"><?php echo $id_bulan ?></option>
                <?php foreach ($this->db->get('bulan')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_bulan ?>"><?php echo $rw->bulan ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Bahan Baku <?php echo form_error('id_bahan_baku') ?></label>
            <!-- <input type="text" class="form-control" name="id_bahan_baku" id="id_bahan_baku" placeholder="Id Bahan Baku" value="<?php echo $id_bahan_baku; ?>" /> -->
            <select name="id_bahan_baku" class="form-control select2">
                <option value="<?php echo $id_bahan_baku ?>"><?php echo $id_bahan_baku ?></option>
                <?php foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_bahan_baku ?>"><?php echo $rw->bahan_baku ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="float">Maksimal <?php echo form_error('maksimal') ?></label>
            <input type="text" class="form-control" name="maksimal" id="maksimal" placeholder="Maksimal" value="<?php echo $maksimal; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Rata Rata <?php echo form_error('rata_rata') ?></label>
            <input type="text" class="form-control" name="rata_rata" id="rata_rata" placeholder="Rata Rata" value="<?php echo $rata_rata; ?>" />
        </div>
	    <input type="hidden" name="id_penggunaan" value="<?php echo $id_penggunaan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('penggunaan_bahan') ?>" class="btn btn-default">Cancel</a>
	</form>
   