
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Bahan Baku <?php echo form_error('bahan_baku') ?></label>
            <input type="text" class="form-control" name="bahan_baku" id="bahan_baku" placeholder="Bahan Baku" value="<?php echo $bahan_baku; ?>" />
        </div>
	    <input type="hidden" name="id_bahan_baku" value="<?php echo $id_bahan_baku; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('bahan_baku') ?>" class="btn btn-default">Cancel</a>
	</form>
   