<div class="row">
	<div class="col-md-12">
		<p>
			<a href="semua_bahanbaku" class="btn btn-primary">Tambah Data</a>
		</p>
		<div class="table-responsive">
		<table class="table table-bordered" id="example">
			<thead>
				<tr>
					<th rowspan="2" width="50">No.</th>
					<th rowspan="2">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td><?php echo $rw->bahan_baku ?></td>
						<?php endforeach ?>
					</tr>

				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>
					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('semua_bahanbaku',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php echo $value->nilai ?></td>
					<?php endforeach ?>

				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>
	</div>

	<hr>
	
	<div class="col-md-12">
		<div class="table-responsive">
		<table class="table table-bordered" id="example">
			<thead>
				<tr>
					<th rowspan="2" width="50">No.</th>
					<th rowspan="2">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td><?php echo "H (". $rw->bahan_baku.")"; ?></td>
						<?php endforeach ?>
					</tr>

				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>

					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('semua_bahanbaku',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php echo number_format(get_H($rw->id_bulan,$value->id_bahan_baku),2) ?></td>
					<?php endforeach ?>

					<!-- <?php 
					$sql = "SELECT a.nilai,b.perbulan FROM semua_bahanbaku as a, penyimpanan_bahan as b WHERE a.id_bahan_baku=b.id_bahan_baku and id_bulan='$rw->id_bulan'";
					foreach ($this->db->query($sql)->result() as $key => $value): ?>
						<td><?php $n = $value->perbulan/$value->nilai; 
						echo number_format($n,2); 
					// echo "$value->perbulan/$value->nilai = $n"; ?></td>
					<?php endforeach ?> -->

				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>
	</div>

</div>