
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('semua_bahanbaku/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('semua_bahanbaku/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('semua_bahanbaku'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Bulan</th>
        <th>Bahan Baku</th>
		<th>Nilai</th>
		<th>Action</th>
            </tr><?php
            foreach ($semua_bahanbaku_data as $semua_bahanbaku)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo get_data('bulan','id_bulan',$semua_bahanbaku->id_bulan,'bulan') ?></td>
            <td><?php echo get_data('bahan_baku','id_bahan_baku',$semua_bahanbaku->id_bahan_baku,'bahan_baku') ?></td>
			<td><?php echo number_format($semua_bahanbaku->nilai,2) ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('semua_bahanbaku/update/'.$semua_bahanbaku->id_d),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('semua_bahanbaku/delete/'.$semua_bahanbaku->id_d),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
        </table>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    