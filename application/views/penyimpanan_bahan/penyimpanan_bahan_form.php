
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Bahan Baku <?php echo form_error('id_bahan_baku') ?></label>
            <!-- <input type="text" class="form-control" name="id_bahan_baku" id="id_bahan_baku" placeholder="Id Bahan Baku" value="<?php echo $id_bahan_baku; ?>" /> -->
            <select name="id_bahan_baku" class="form-control select2">
                <option value="<?php echo $id_bahan_baku ?>"><?php echo $id_bahan_baku ?></option>
                <?php foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
                    <option value="<?php echo $rw->id_bahan_baku ?>"><?php echo $rw->bahan_baku ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Biaya Simpan <?php echo form_error('biaya_simpan') ?></label>
            <input type="text" class="form-control" name="biaya_simpan" id="biaya_simpan" placeholder="Biaya Simpan" value="<?php echo $biaya_simpan; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Total Biaya Simpan <?php echo form_error('total_biaya_simpan') ?></label>
            <input type="text" class="form-control" name="total_biaya_simpan" id="total_biaya_simpan" placeholder="Total Biaya Simpan" value="<?php echo $total_biaya_simpan; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Biaya Penyimpanan <?php echo form_error('biaya_penyimpanan') ?></label>
            <input type="text" class="form-control" name="biaya_penyimpanan" id="biaya_penyimpanan" placeholder="Biaya Penyimpanan" value="<?php echo $biaya_penyimpanan; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Perbulan <?php echo form_error('perbulan') ?></label>
            <input type="text" class="form-control" name="perbulan" id="perbulan" placeholder="Perbulan" value="<?php echo $perbulan; ?>" />
        </div>
	    <input type="hidden" name="id_penyimpanan" value="<?php echo $id_penyimpanan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('penyimpanan_bahan') ?>" class="btn btn-default">Cancel</a>
	</form>
   