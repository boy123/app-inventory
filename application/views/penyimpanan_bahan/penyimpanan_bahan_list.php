
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <?php echo anchor(site_url('penyimpanan_bahan/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                <form action="<?php echo site_url('penyimpanan_bahan/index'); ?>" class="form-inline" method="get">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" value="<?php echo $q; ?>">
                        <span class="input-group-btn">
                            <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('penyimpanan_bahan'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                          <button class="btn btn-primary" type="submit">Search</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Bahan Baku</th>
		<th>Biaya Simpan</th>
		<th>Total Biaya Simpan</th>
		<th>Biaya Penyimpanan</th>
		<th>Perbulan</th>
		<th>Action</th>
            </tr><?php
            $total = 0;
            $total_biaya = 0;
            $total_perbulan = 0;
            foreach ($penyimpanan_bahan_data as $penyimpanan_bahan)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo get_data('bahan_baku','id_bahan_baku',$penyimpanan_bahan->id_bahan_baku,'bahan_baku') ?></td>
			<td><?php echo $penyimpanan_bahan->biaya_simpan;$total=$total+$penyimpanan_bahan->biaya_simpan; ?></td>
			<td><?php echo number_format($penyimpanan_bahan->total_biaya_simpan); ?></td>
			<td><?php echo number_format($penyimpanan_bahan->biaya_penyimpanan); $total_biaya=$total_biaya+$penyimpanan_bahan->biaya_penyimpanan; ?></td>
			<td><?php echo number_format($penyimpanan_bahan->perbulan); $total_perbulan=$total_perbulan+$penyimpanan_bahan->perbulan; ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('penyimpanan_bahan/update/'.$penyimpanan_bahan->id_penyimpanan),'<span class="label label-info">Ubah</span>'); 
				echo ' | '; 
				echo anchor(site_url('penyimpanan_bahan/delete/'.$penyimpanan_bahan->id_penyimpanan),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
            <tr>
                <td colspan="2">
                    <b>Total</b>
                </td>
                <td><?php echo $total ?></td>
                <td></td>
                <td>
                    <?php echo number_format($total_biaya) ?>
                </td>
                <td>
                    <?php echo number_format($total_perbulan) ?>
                </td>
            </tr>
        </table>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="#" class="btn btn-primary">Total Record : <?php echo $total_rows ?></a>
	    </div>
            <div class="col-md-6 text-right">
                <?php echo $pagination ?>
            </div>
        </div>
    