<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success">
			<h2>Selamat datang, <?php echo $this->session->userdata('username'); ?></h2>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-md-12">
		<div id="chartContainer" style="height: 300px; width: 100%;"></div>
	</div>
</div>



<div class="row">
	<div class="col-md-12">
		<div id="chartContainer2" style="height: 300px; width: 100%;"></div>
	</div>
</div>

<script>
		window.onload = function () {

		var chart = new CanvasJS.Chart("chartContainer", {
			exportEnabled: true,
			animationEnabled: true,
			title:{
				text: "Tahap VI"
			},
			subtitles: [{
				text: ""
			}], 
			axisX: {
				title: "Bahan Baku"
			},
			axisY: {
				title: "",
				titleFontColor: "#4F81BC",
				lineColor: "#4F81BC",
				labelFontColor: "#4F81BC",
				tickColor: "#4F81BC"
			},
			axisY2: {
				title: "",
				titleFontColor: "#C0504E",
				lineColor: "#C0504E",
				labelFontColor: "#C0504E",
				tickColor: "#C0504E"
			},
			toolTip: {
				shared: true
			},
			legend: {
				cursor: "pointer",
				itemclick: toggleDataSeries
			},
			data: [

			<?php 
			$this->db->order_by('id_bahan_baku', 'asc');
			foreach ($this->db->get('bahan_baku')->result() as $bhn): ?>
			{
				type: "column",
				name: "<?php echo $bhn->bahan_baku ?>",
				showInLegend: true,      
				yValueFormatString: "#,##0.#",
				dataPoints: [

					<?php foreach ($this->db->get('bulan')->result() as $rw): ?>
						
					{ label: "<?php echo $rw->bulan ?>",  y: <?php echo get_tahap6($rw->id_bulan,$bhn->id_bahan_baku) ?> },

					<?php endforeach ?>
				]
			},

			<?php endforeach ?>

			

			]
		});
		chart.render();

		var chart1 = new CanvasJS.Chart("chartContainer2", {
			exportEnabled: true,
			animationEnabled: true,
			title:{
				text: "Selisih"
			},
			subtitles: [{
				text: ""
			}], 
			axisX: {
				title: "Bahan Baku"
			},
			axisY: {
				title: "",
				titleFontColor: "#4F81BC",
				lineColor: "#4F81BC",
				labelFontColor: "#4F81BC",
				tickColor: "#4F81BC"
			},
			axisY2: {
				title: "",
				titleFontColor: "#C0504E",
				lineColor: "#C0504E",
				labelFontColor: "#C0504E",
				tickColor: "#C0504E"
			},
			toolTip: {
				shared: true
			},
			legend: {
				cursor: "pointer",
				itemclick: toggleDataSeries2
			},
			data: [

			<?php 
			$this->db->order_by('id_bahan_baku', 'asc');
			foreach ($this->db->get('bahan_baku')->result() as $bhn): ?>
			{
				type: "column",
				name: "<?php echo $bhn->bahan_baku ?>",
				showInLegend: true,      
				yValueFormatString: "#,##0.#",
				dataPoints: [

					<?php foreach ($this->db->get('bulan')->result() as $rw): ?>
						
					{ label: "<?php echo $rw->bulan ?>",  y: <?php echo get_tahap_selisih($rw->id_bulan,$bhn->id_bahan_baku) ?> },

					<?php endforeach ?>
				]
			},

			<?php endforeach ?>

			

			]
		});
		chart1.render();

		function toggleDataSeries2(e) {
			if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
				e.dataSeries.visible = false;
			} else {
				e.dataSeries.visible = true;
			}
			e.chart.render();
		}

		function toggleDataSeries(e) {
			if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
				e.dataSeries.visible = false;
			} else {
				e.dataSeries.visible = true;
			}
			e.chart.render();
		}

		}
		</script>
		<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>