<div id="cetak">
<div class="row">
	<div class="col-md-12">
		<h2>TAHAP I (Mencari EOQ)</h2>
		<div class="table-responsive">
		<table class="table table-bordered" id="example">
			<thead>
				<tr>
					<th rowspan="2" width="50">No.</th>
					<th rowspan="2">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td><?php echo $rw->bahan_baku ?></td>
						<?php endforeach ?>
					</tr>

				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>
					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('semua_bahanbaku',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php 
							echo number_format(get_tahap1($rw->id_bulan,$value->id_bahan_baku),2); 
							// echo get_tahap1($rw->id_bulan,$value->id_bahan_baku); 

							?>
						
						</td>
					<?php endforeach ?>

				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<h2>TAHAP II (Mencari Safety Stock)</h2>
		<div class="table-responsive">
		<table class="table table-bordered" id="example">
			<thead>
				<tr>
					<th rowspan="2" width="50">No.</th>
					<th rowspan="2">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td><?php echo $rw->bahan_baku ?></td>
						<?php endforeach ?>
					</tr>

				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>
					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('semua_bahanbaku',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php 
							echo number_format(get_tahap2($rw->id_bulan,$value->id_bahan_baku),2)
							// echo get_tahap2($rw->id_bulan,$value->id_bahan_baku);

							?>
						
						</td>
					<?php endforeach ?>

				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>TAHAP III (Mencari d (tingkat kebutuhan per unit waktu))</h2>
		<div class="table-responsive">
		<table class="table table-bordered" id="example">
			<thead>
				<tr>
					<th rowspan="2" width="50">No.</th>
					<th rowspan="2">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td><?php echo $rw->bahan_baku ?></td>
						<?php endforeach ?>
					</tr>

				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>
					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('semua_bahanbaku',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php 
							echo number_format(get_tahap3a($rw->id_bulan,$value->id_bahan_baku),2)

							?>
						
						</td>
					<?php endforeach ?>

				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>

		<h2>(Mencari ROP)</h2>
		<div class="table-responsive">
		<table class="table table-bordered" id="example">
			<thead>
				<tr>
					<th rowspan="2" width="50">No.</th>
					<th rowspan="2">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td><?php echo $rw->bahan_baku ?></td>
						<?php endforeach ?>
					</tr>

				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>
					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('semua_bahanbaku',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php 
							echo number_format(get_tahap3b($rw->id_bulan,$value->id_bahan_baku),2)

							?>
						
						</td>
					<?php endforeach ?>

				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>TAHAP IV (Mencari Frekuensi pemesanan)</h2>
		<div class="table-responsive">
		<table class="table table-bordered" id="example">
			<thead>
				<tr>
					<th rowspan="2" width="50">No.</th>
					<th rowspan="2">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td><?php echo $rw->bahan_baku ?></td>
						<?php endforeach ?>
					</tr>

				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>
					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('semua_bahanbaku',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php 
							echo number_format(get_tahap4($rw->id_bulan,$value->id_bahan_baku),2)

							?>
						
						</td>
					<?php endforeach ?>

				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>TAHAP V (Mencari TIC EOQ)</h2>
		<div class="table-responsive">
		<table class="table table-bordered" id="example">
			<thead>
				<tr>
					<th rowspan="2" width="50">No.</th>
					<th rowspan="2">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td><?php echo $rw->bahan_baku ?></td>
						<?php endforeach ?>
					</tr>

				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>
					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('semua_bahanbaku',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php 
							echo number_format(get_tahap5($rw->id_bulan,$value->id_bahan_baku),2)

							?>
						
						</td>
					<?php endforeach ?>

				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>TAHAP VI (Mencari TIC)</h2>
		<div class="table-responsive">
		<table class="table table-bordered" id="example">
			<thead>
				<tr>
					<th rowspan="2" width="50">No.</th>
					<th rowspan="2">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td><?php echo $rw->bahan_baku ?></td>
						<?php endforeach ?>
					</tr>

				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>
					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('semua_bahanbaku',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php 
							echo number_format(get_tahap6($rw->id_bulan,$value->id_bahan_baku),2)

							?>
						
						</td>
					<?php endforeach ?>

				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<h2>SELISIH</h2>
		<div class="table-responsive">
		<table class="table table-bordered" id="example">
			<thead>
				<tr>
					<th rowspan="2" width="50">No.</th>
					<th rowspan="2">Bulan</th>
					<th colspan="7" style="text-align: center;">Bahan Baku</th>
					<tr style="text-align: center;">
						<?php 
						$this->db->order_by('id_bahan_baku', 'asc');
						foreach ($this->db->get('bahan_baku')->result() as $rw): ?>
							<td><?php echo $rw->bahan_baku ?></td>
						<?php endforeach ?>
					</tr>

				</tr>
			</thead>
			<tbody>
				<?php 
				$no=1;
				foreach ($this->db->get('bulan')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo $rw->bulan ?></td>
					<?php 
					$this->db->order_by('id_bahan_baku', 'asc');
					foreach ($this->db->get_where('semua_bahanbaku',array('id_bulan'=>$rw->id_bulan))->result() as $key => $value): ?>
						<td><?php 
							echo number_format(get_tahap_selisih($rw->id_bulan,$value->id_bahan_baku),2)

							?>
						
						</td>
					<?php endforeach ?>

				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
		</div>
	</div>
</div>

</div>
<div class="row">
	<div class="col-md-12">
		<button onclick="printContent('cetak')" class="btn btn-primary">Print Sekarang</button>
	</div>
</div>

<script>
function printContent(el){
	var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(el).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;
}
</script>