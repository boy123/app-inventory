<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="image/user/<?php echo $this->session->userdata('foto'); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('nama'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <?php if ($this->session->userdata('level') == 'admin'){ ?>
        <li><a href="app"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
        
        <li><a href="bahan_baku"><i class="fa fa-cube"></i> <span>Master Bahan Baku</span></a></li>

        <li><a href="app/data_bahanbaku"><i class="fa fa-file"></i> <span>Bahan Baku Tahunan</span></a></li>

        <li><a href="pemesanan_bahan"><i class="fa fa-file"></i> <span>Pemesanan Bahan Baku</span></a></li>

        <li><a href="penyimpanan_bahan"><i class="fa fa-file"></i> <span>Penyimpanan Bahan Baku</span></a></li>

        <li><a href="app/penggunaan_bahanbaku"><i class="fa fa-file"></i> <span>Penggunaan Bahan Baku</span></a></li>
        <li><a href="app/tahapan"><i class="fa fa-step-forward"></i> <span>Analisa Tahapan</span></a></li>
        
        <li><a href="a_user"><i class="fa fa-users"></i> <span>Master User</span></a></li>
        

        <?php } ?>
        
        

        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Faqs</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>About</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>