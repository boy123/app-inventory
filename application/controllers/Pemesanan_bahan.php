<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pemesanan_bahan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Pemesanan_bahan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'pemesanan_bahan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'pemesanan_bahan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'pemesanan_bahan/index.html';
            $config['first_url'] = base_url() . 'pemesanan_bahan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Pemesanan_bahan_model->total_rows($q);
        $pemesanan_bahan = $this->Pemesanan_bahan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'pemesanan_bahan_data' => $pemesanan_bahan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'pemesanan_bahan/pemesanan_bahan_list',
            'konten' => 'pemesanan_bahan/pemesanan_bahan_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Pemesanan_bahan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_pemesanan' => $row->id_pemesanan,
		'id_bahan_baku' => $row->id_bahan_baku,
		'frekuensi' => $row->frekuensi,
		'biaya_transportasi' => $row->biaya_transportasi,
		'total_biaya' => $row->total_biaya,
		'lead_time' => $row->lead_time,
	    );
            $this->load->view('pemesanan_bahan/pemesanan_bahan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pemesanan_bahan'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'pemesanan_bahan/pemesanan_bahan_form',
            'konten' => 'pemesanan_bahan/pemesanan_bahan_form',
            'button' => 'Create',
            'action' => site_url('pemesanan_bahan/create_action'),
	    'id_pemesanan' => set_value('id_pemesanan'),
	    'id_bahan_baku' => set_value('id_bahan_baku'),
	    'frekuensi' => set_value('frekuensi'),
	    'biaya_transportasi' => set_value('biaya_transportasi'),
	    'total_biaya' => set_value('total_biaya'),
	    'lead_time' => set_value('lead_time'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_bahan_baku' => $this->input->post('id_bahan_baku',TRUE),
		'frekuensi' => $this->input->post('frekuensi',TRUE),
		'biaya_transportasi' => $this->input->post('biaya_transportasi',TRUE),
		'total_biaya' => $this->input->post('total_biaya',TRUE),
		'lead_time' => $this->input->post('lead_time',TRUE),
	    );

            $this->Pemesanan_bahan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pemesanan_bahan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pemesanan_bahan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'pemesanan_bahan/pemesanan_bahan_form',
                'konten' => 'pemesanan_bahan/pemesanan_bahan_form',
                'button' => 'Update',
                'action' => site_url('pemesanan_bahan/update_action'),
		'id_pemesanan' => set_value('id_pemesanan', $row->id_pemesanan),
		'id_bahan_baku' => set_value('id_bahan_baku', $row->id_bahan_baku),
		'frekuensi' => set_value('frekuensi', $row->frekuensi),
		'biaya_transportasi' => set_value('biaya_transportasi', $row->biaya_transportasi),
		'total_biaya' => set_value('total_biaya', $row->total_biaya),
		'lead_time' => set_value('lead_time', $row->lead_time),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pemesanan_bahan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pemesanan', TRUE));
        } else {
            $data = array(
		'id_bahan_baku' => $this->input->post('id_bahan_baku',TRUE),
		'frekuensi' => $this->input->post('frekuensi',TRUE),
		'biaya_transportasi' => $this->input->post('biaya_transportasi',TRUE),
		'total_biaya' => $this->input->post('total_biaya',TRUE),
		'lead_time' => $this->input->post('lead_time',TRUE),
	    );

            $this->Pemesanan_bahan_model->update($this->input->post('id_pemesanan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pemesanan_bahan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pemesanan_bahan_model->get_by_id($id);

        if ($row) {
            $this->Pemesanan_bahan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pemesanan_bahan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pemesanan_bahan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_bahan_baku', 'id bahan baku', 'trim|required');
	$this->form_validation->set_rules('frekuensi', 'frekuensi', 'trim|required');
	$this->form_validation->set_rules('biaya_transportasi', 'biaya transportasi', 'trim|required');
	$this->form_validation->set_rules('total_biaya', 'total biaya', 'trim|required');
	$this->form_validation->set_rules('lead_time', 'lead time', 'trim|required');

	$this->form_validation->set_rules('id_pemesanan', 'id_pemesanan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Pemesanan_bahan.php */
/* Location: ./application/controllers/Pemesanan_bahan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-07-05 04:33:46 */
/* https://jualkoding.com */