<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penggunaan_bahan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penggunaan_bahan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'penggunaan_bahan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'penggunaan_bahan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'penggunaan_bahan/index.html';
            $config['first_url'] = base_url() . 'penggunaan_bahan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Penggunaan_bahan_model->total_rows($q);
        $penggunaan_bahan = $this->Penggunaan_bahan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'penggunaan_bahan_data' => $penggunaan_bahan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'penggunaan_bahan/penggunaan_bahan_list',
            'konten' => 'penggunaan_bahan/penggunaan_bahan_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Penggunaan_bahan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_penggunaan' => $row->id_penggunaan,
		'id_bulan' => $row->id_bulan,
		'id_bahan_baku' => $row->id_bahan_baku,
		'maksimal' => $row->maksimal,
		'rata_rata' => $row->rata_rata,
	    );
            $this->load->view('penggunaan_bahan/penggunaan_bahan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penggunaan_bahan'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'penggunaan_bahan/penggunaan_bahan_form',
            'konten' => 'penggunaan_bahan/penggunaan_bahan_form',
            'button' => 'Create',
            'action' => site_url('penggunaan_bahan/create_action'),
	    'id_penggunaan' => set_value('id_penggunaan'),
	    'id_bulan' => set_value('id_bulan'),
	    'id_bahan_baku' => set_value('id_bahan_baku'),
	    'maksimal' => set_value('maksimal'),
	    'rata_rata' => set_value('rata_rata'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_bulan' => $this->input->post('id_bulan',TRUE),
		'id_bahan_baku' => $this->input->post('id_bahan_baku',TRUE),
		'maksimal' => $this->input->post('maksimal',TRUE),
		'rata_rata' => $this->input->post('rata_rata',TRUE),
	    );

            $this->Penggunaan_bahan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('penggunaan_bahan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Penggunaan_bahan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'penggunaan_bahan/penggunaan_bahan_form',
                'konten' => 'penggunaan_bahan/penggunaan_bahan_form',
                'button' => 'Update',
                'action' => site_url('penggunaan_bahan/update_action'),
		'id_penggunaan' => set_value('id_penggunaan', $row->id_penggunaan),
		'id_bulan' => set_value('id_bulan', $row->id_bulan),
		'id_bahan_baku' => set_value('id_bahan_baku', $row->id_bahan_baku),
		'maksimal' => set_value('maksimal', $row->maksimal),
		'rata_rata' => set_value('rata_rata', $row->rata_rata),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penggunaan_bahan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_penggunaan', TRUE));
        } else {
            $data = array(
		'id_bulan' => $this->input->post('id_bulan',TRUE),
		'id_bahan_baku' => $this->input->post('id_bahan_baku',TRUE),
		'maksimal' => $this->input->post('maksimal',TRUE),
		'rata_rata' => $this->input->post('rata_rata',TRUE),
	    );

            $this->Penggunaan_bahan_model->update($this->input->post('id_penggunaan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('penggunaan_bahan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Penggunaan_bahan_model->get_by_id($id);

        if ($row) {
            $this->Penggunaan_bahan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('penggunaan_bahan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penggunaan_bahan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_bulan', 'id bulan', 'trim|required');
	$this->form_validation->set_rules('id_bahan_baku', 'id bahan baku', 'trim|required');
	$this->form_validation->set_rules('maksimal', 'maksimal', 'trim|required');
	$this->form_validation->set_rules('rata_rata', 'rata rata', 'trim|required');

	$this->form_validation->set_rules('id_penggunaan', 'id_penggunaan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Penggunaan_bahan.php */
/* Location: ./application/controllers/Penggunaan_bahan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-07-05 04:34:03 */
/* https://jualkoding.com */