<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

	public function tes()
    {
        $a = sqrt(2*250000*312.75/(21969000/312.75));
        echo $a;
    }
	public function index()
	{
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
		$data = array(
			'konten' => 'home_admin',
            'judul_page' => 'Dashboard',
		);
		$this->load->view('v_index', $data);
    }

    public function data_bahanbaku()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        $data = array(
            'konten' => 'semua_bahanbaku/view',
            'judul_page' => 'Data Bahan Baku',
        );
        $this->load->view('v_index', $data);
    }

    public function penggunaan_bahanbaku()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        $data = array(
            'konten' => 'penggunaan_bahan/view',
            'judul_page' => 'Penggunaan Data setiap bulan',
        );
        $this->load->view('v_index', $data);
    }

    public function tahapan()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        $data = array(
            'konten' => 'tahap/view',
            'judul_page' => 'Tahapan',
        );
        $this->load->view('v_index', $data);
    }

    

}
