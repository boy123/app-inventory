<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penyimpanan_bahan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penyimpanan_bahan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'penyimpanan_bahan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'penyimpanan_bahan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'penyimpanan_bahan/index.html';
            $config['first_url'] = base_url() . 'penyimpanan_bahan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Penyimpanan_bahan_model->total_rows($q);
        $penyimpanan_bahan = $this->Penyimpanan_bahan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'penyimpanan_bahan_data' => $penyimpanan_bahan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'penyimpanan_bahan/penyimpanan_bahan_list',
            'konten' => 'penyimpanan_bahan/penyimpanan_bahan_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Penyimpanan_bahan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_penyimpanan' => $row->id_penyimpanan,
		'id_bahan_baku' => $row->id_bahan_baku,
		'biaya_simpan' => $row->biaya_simpan,
		'total_biaya_simpan' => $row->total_biaya_simpan,
		'biaya_penyimpanan' => $row->biaya_penyimpanan,
		'perbulan' => $row->perbulan,
	    );
            $this->load->view('penyimpanan_bahan/penyimpanan_bahan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penyimpanan_bahan'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'penyimpanan_bahan/penyimpanan_bahan_form',
            'konten' => 'penyimpanan_bahan/penyimpanan_bahan_form',
            'button' => 'Create',
            'action' => site_url('penyimpanan_bahan/create_action'),
	    'id_penyimpanan' => set_value('id_penyimpanan'),
	    'id_bahan_baku' => set_value('id_bahan_baku'),
	    'biaya_simpan' => set_value('biaya_simpan'),
	    'total_biaya_simpan' => set_value('total_biaya_simpan'),
	    'biaya_penyimpanan' => set_value('biaya_penyimpanan'),
	    'perbulan' => set_value('perbulan'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_bahan_baku' => $this->input->post('id_bahan_baku',TRUE),
		'biaya_simpan' => $this->input->post('biaya_simpan',TRUE),
		'total_biaya_simpan' => $this->input->post('total_biaya_simpan',TRUE),
		'biaya_penyimpanan' => $this->input->post('biaya_penyimpanan',TRUE),
		'perbulan' => $this->input->post('perbulan',TRUE),
	    );

            $this->Penyimpanan_bahan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('penyimpanan_bahan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Penyimpanan_bahan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'penyimpanan_bahan/penyimpanan_bahan_form',
                'konten' => 'penyimpanan_bahan/penyimpanan_bahan_form',
                'button' => 'Update',
                'action' => site_url('penyimpanan_bahan/update_action'),
		'id_penyimpanan' => set_value('id_penyimpanan', $row->id_penyimpanan),
		'id_bahan_baku' => set_value('id_bahan_baku', $row->id_bahan_baku),
		'biaya_simpan' => set_value('biaya_simpan', $row->biaya_simpan),
		'total_biaya_simpan' => set_value('total_biaya_simpan', $row->total_biaya_simpan),
		'biaya_penyimpanan' => set_value('biaya_penyimpanan', $row->biaya_penyimpanan),
		'perbulan' => set_value('perbulan', $row->perbulan),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penyimpanan_bahan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_penyimpanan', TRUE));
        } else {
            $data = array(
		'id_bahan_baku' => $this->input->post('id_bahan_baku',TRUE),
		'biaya_simpan' => $this->input->post('biaya_simpan',TRUE),
		'total_biaya_simpan' => $this->input->post('total_biaya_simpan',TRUE),
		'biaya_penyimpanan' => $this->input->post('biaya_penyimpanan',TRUE),
		'perbulan' => $this->input->post('perbulan',TRUE),
	    );

            $this->Penyimpanan_bahan_model->update($this->input->post('id_penyimpanan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('penyimpanan_bahan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Penyimpanan_bahan_model->get_by_id($id);

        if ($row) {
            $this->Penyimpanan_bahan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('penyimpanan_bahan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penyimpanan_bahan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_bahan_baku', 'id bahan baku', 'trim|required');
	$this->form_validation->set_rules('biaya_simpan', 'biaya simpan', 'trim|required');
	$this->form_validation->set_rules('total_biaya_simpan', 'total biaya simpan', 'trim|required');
	$this->form_validation->set_rules('biaya_penyimpanan', 'biaya penyimpanan', 'trim|required');
	$this->form_validation->set_rules('perbulan', 'perbulan', 'trim|required');

	$this->form_validation->set_rules('id_penyimpanan', 'id_penyimpanan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Penyimpanan_bahan.php */
/* Location: ./application/controllers/Penyimpanan_bahan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-07-05 04:57:51 */
/* https://jualkoding.com */