<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Semua_bahanbaku extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Semua_bahanbaku_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'semua_bahanbaku/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'semua_bahanbaku/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'semua_bahanbaku/index.html';
            $config['first_url'] = base_url() . 'semua_bahanbaku/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Semua_bahanbaku_model->total_rows($q);
        $semua_bahanbaku = $this->Semua_bahanbaku_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'semua_bahanbaku_data' => $semua_bahanbaku,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'semua_bahanbaku/semua_bahanbaku_list',
            'konten' => 'semua_bahanbaku/semua_bahanbaku_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Semua_bahanbaku_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_d' => $row->id_d,
		'id_bulan' => $row->id_bulan,
		'id_bahan_baku' => $row->id_bahan_baku,
	    );
            $this->load->view('semua_bahanbaku/semua_bahanbaku_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('semua_bahanbaku'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'semua_bahanbaku/semua_bahanbaku_form',
            'konten' => 'semua_bahanbaku/semua_bahanbaku_form',
            'button' => 'Create',
            'action' => site_url('semua_bahanbaku/create_action'),
	    'id_d' => set_value('id_d'),
	    'id_bulan' => set_value('id_bulan'),
        'id_bahan_baku' => set_value('id_bahan_baku'),
	    'nilai' => set_value('nilai'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_bulan' => $this->input->post('id_bulan',TRUE),
        'id_bahan_baku' => $this->input->post('id_bahan_baku',TRUE),
		'nilai' => $this->input->post('nilai',TRUE),
	    );

            $this->Semua_bahanbaku_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('semua_bahanbaku'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Semua_bahanbaku_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'semua_bahanbaku/semua_bahanbaku_form',
                'konten' => 'semua_bahanbaku/semua_bahanbaku_form',
                'button' => 'Update',
                'action' => site_url('semua_bahanbaku/update_action'),
		'id_d' => set_value('id_d', $row->id_d),
		'id_bulan' => set_value('id_bulan', $row->id_bulan),
        'id_bahan_baku' => set_value('id_bahan_baku', $row->id_bahan_baku),
		'nilai' => set_value('nilai', $row->nilai),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('semua_bahanbaku'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_d', TRUE));
        } else {
            $data = array(
		'id_bulan' => $this->input->post('id_bulan',TRUE),
        'id_bahan_baku' => $this->input->post('id_bahan_baku',TRUE),
		'nilai' => $this->input->post('nilai',TRUE),
	    );

            $this->Semua_bahanbaku_model->update($this->input->post('id_d', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('semua_bahanbaku'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Semua_bahanbaku_model->get_by_id($id);

        if ($row) {
            $this->Semua_bahanbaku_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('semua_bahanbaku'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('semua_bahanbaku'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_bulan', 'id bulan', 'trim|required');
	$this->form_validation->set_rules('id_bahan_baku', 'id bahan baku', 'trim|required');

	$this->form_validation->set_rules('id_d', 'id_d', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Semua_bahanbaku.php */
/* Location: ./application/controllers/Semua_bahanbaku.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-06-25 09:39:04 */
/* https://jualkoding.com */