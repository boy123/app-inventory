<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bahan_baku extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Bahan_baku_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'bahan_baku/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'bahan_baku/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'bahan_baku/index.html';
            $config['first_url'] = base_url() . 'bahan_baku/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Bahan_baku_model->total_rows($q);
        $bahan_baku = $this->Bahan_baku_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'bahan_baku_data' => $bahan_baku,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'bahan_baku/bahan_baku_list',
            'konten' => 'bahan_baku/bahan_baku_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Bahan_baku_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_bahan_baku' => $row->id_bahan_baku,
		'bahan_baku' => $row->bahan_baku,
	    );
            $this->load->view('bahan_baku/bahan_baku_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('bahan_baku'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'bahan_baku/bahan_baku_form',
            'konten' => 'bahan_baku/bahan_baku_form',
            'button' => 'Create',
            'action' => site_url('bahan_baku/create_action'),
	    'id_bahan_baku' => set_value('id_bahan_baku'),
	    'bahan_baku' => set_value('bahan_baku'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'bahan_baku' => $this->input->post('bahan_baku',TRUE),
	    );

            $this->Bahan_baku_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('bahan_baku'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Bahan_baku_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'bahan_baku/bahan_baku_form',
                'konten' => 'bahan_baku/bahan_baku_form',
                'button' => 'Update',
                'action' => site_url('bahan_baku/update_action'),
		'id_bahan_baku' => set_value('id_bahan_baku', $row->id_bahan_baku),
		'bahan_baku' => set_value('bahan_baku', $row->bahan_baku),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('bahan_baku'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_bahan_baku', TRUE));
        } else {
            $data = array(
		'bahan_baku' => $this->input->post('bahan_baku',TRUE),
	    );

            $this->Bahan_baku_model->update($this->input->post('id_bahan_baku', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('bahan_baku'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Bahan_baku_model->get_by_id($id);

        if ($row) {
            $this->Bahan_baku_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('bahan_baku'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('bahan_baku'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('bahan_baku', 'bahan baku', 'trim|required');

	$this->form_validation->set_rules('id_bahan_baku', 'id_bahan_baku', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Bahan_baku.php */
/* Location: ./application/controllers/Bahan_baku.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2020-06-25 09:26:52 */
/* https://jualkoding.com */